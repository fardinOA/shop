import React from 'react';
import team_member from '../../teamMember'
function TeamMembers() {
  return (
      <div className="team-our-team mt-5">
          <div className="container">
              <div className="text-center p mb-5">
                  <h1>Meet Our Team</h1>
                  <p className="text-green">meet some of our dedicated team member</p>
              </div>
              <div className="row">
                  {
                      team_member.map((ele, ind) => (
                          <div key={ind} className="col-sm-12 col-lg-3 mb-5">
                              <div className="card ">
                                  <div className="img">
                                      <img src={ele.image} alt="" />
                                  </div>
                                  <div className="card-title text-center">
                                      <h3>{ele.name}</h3>
                                      <p>{ele.post}</p>
                                  </div>
                              </div>
                          </div>
                      ))
                  }

              </div>
          </div>
      </div>
  );
}

export default TeamMembers;
