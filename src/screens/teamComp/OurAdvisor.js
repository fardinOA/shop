import React from 'react';

function OurAdvisor() {
  return (
      <div className="our-advisor ">
          <div className="container">
              <div className="text-center mb-5">
                  <h1>Our Advisor</h1>
              </div>
              <div className="advisors">
                  <div className="row">
                      <div className="col-sm-12 col-lg-4">
                          <div className="card ">
                              <div className="img">
                                  <img src="image/advisor1.jpg" alt="" />
                              </div>
                              <div className="text-center">
                                  <h3>S M Arifuzzaman</h3>
                                  <p>Advison</p>
                              </div>
                          </div>
                      </div>

                      <div className="col-sm-12 col-lg-4">
                          <div className="card ">
                              <div className="img">
                                  <img src="image/advisor2.jpg" alt="" />
                              </div>
                              <div className="text-center">
                                  <h3>S M Arifuzzaman</h3>
                                  <p>Advison</p>
                              </div>
                          </div>
                      </div>
                  </div>

              </div>
          </div>
      </div>
  );
}

export default OurAdvisor;
