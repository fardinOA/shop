import React from 'react';
import team_member from '../../teamMember'
function EmployeThought() {
  return (
      <div className="employe-thought p-5">
          <div className="text-center mb-5">
              <h1>Employee Thoughts</h1>
              <p>Here are some of our employees who has shared
                  their thoughts on <span>Shopkpr</span> </p>
          </div>

          <div className="container">

              <div className="row">

                  {
                      team_member.map((ele, ind) => (

                          (ele.name == 'Shah Faiza Zarin') ? null : <div className="col-sm-12 col-lg-3 card mb-5">
                             <div className="content">
                                  <div className="card-headers">
                                      <div className="img">
                                          <img src={ele.image} alt="" />
                                      </div>
                                      <div className="text-center">
                                          <h6>{ele.name}</h6>
                                          <p>{ele.post}</p>
                                      </div>

                                  </div>
                                  <p>{ele.details}</p>
                             </div>
                          </div>

                      ))
                  }
              </div>
          </div>
          </div>
  );
}

export default EmployeThought;
