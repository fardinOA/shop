import React from 'react';
import team_member from '../teamMember'
import Feature from './aboutComp/Feature';
import OurOffice from './aboutComp/OurOffice';
import EmployeThought from './teamComp/EmployeThought';
import OurAdvisor from './teamComp/OurAdvisor';
import TeamHero from './teamComp/TeamHero';
import TeamMembers from './teamComp/TeamMembers';
function Team() {
  return (
    <div>
         <TeamHero />
         <TeamMembers />
         <OurAdvisor />
         <EmployeThought />
         <Feature />
         <OurOffice />

    </div>
  );
}

export default Team;
