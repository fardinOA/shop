import React from 'react';

function OurStory() {
  return (
      <div className="our-story mb-5 mt-4">
          <div className="container">
              <div className="text-center">
                  <h1>Our Story</h1>
                  <p>Our leaders who made shopkpr possible.</p>
              </div>
              <div className="row">
                  <div className="col-sm-12 col-lg-6">
                      <div className="img">
                          <img src="image/about-story.jpg" alt="" />
                      </div>
                  </div>
                  <div className="col-sm-12 col-lg-6  text-lg-left">
                      <h3 className="font-weight-bold">Measbaul ShahedAdor</h3>
                      <p>Co-Founder & CEO</p>
                      <p>“Solve the practical problems that you are facing yourself. That will be the best startup-idea for you.” The concept of ShopKpr started after realizing a problem, after getting stuck on that problem for several days, we came up with ShopKpr as the solution. We come up with ideas, created business plans, and eventually started putting them into action. Our only goal is to solve all of the retailers' major problems and to organize every type of small business stores. We want to radically change Bangladesh's supply chain environment while also growing a significant, meaningful, and lucrative business that will make our country thrive in the supply chain industry. We're working hard, learning, and growing, striving to rise above our circumstances and change the existing quo in a sector that affects millions of people in our country. We'll fight till the end. We'll do it.</p>

                  </div>
              </div>

              <div className="row mt-5">

                  <div className="col-sm-12 col-lg-6  text-lg-left">
                      <h3 className="font-weight-bold">Walid Hasan</h3>
                      <p>Co-Founder & COO</p>
                      <p>365 days can create 365 opportunities. Shopkpr just happend to be one of those opportunities for me. The role of some positive and inspiring people in our country is the reason behind the confidence to be an entrepreneur. Shopkpr started it’s journey with a small Dream. Little by little, that dream became bigger. The path to reach that dream was not easy, there were a lot of obstacles and barriers we had to face. One of the most important hardest role is being the co-founder of Shopkpr who is one of the metaphors for our progress. Shopkpr is now visible and has a long way to go. I believe that nothing in the world is permanent, not even our problems. It may not be easy to move forward, but I must keep pushing myself. We don't know what will happen tomorrow, so let's move on without losing hope. The Shopkpr family is my strength which always reminds me, something good is waiting for us.</p>
                  </div>
                  <div className="col-sm-12 col-lg-6">
                      <div className="img">
                          <img src="image/about-story-2.jpg" alt="" />
                      </div>
                  </div>

              </div>

          </div>
      </div>
  );
}

export default OurStory;
