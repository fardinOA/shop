import React from 'react';

function Feature() {
  return (
      <div className="feature-service">
          <div className="container">
              <div className="row mb-5">
                  <div className="col-sm-12 col-lg-6  ">
                      <h1>Feature Service</h1>
                      <p>Our feature services primarily focus on 3 segments: Garments, Book publishers and Small local businesses.</p>
                      <p>Our customers will get different types of benefits from us. For example,they will be given a smart profile through which they will be able to order and maintain their contact with us. For their audit, they will also receive free digital khata. “ShopKpr” will offer low-cost logistic and credit support. Also for local brand developments, we will give our full support. Another unique aspect will be the organization of leftover product recycling</p>
                      <button className="btn btn-lg">Get Started</button>
                  </div>
                  <div className="col-sm-12 col-lg-6">
                      <div className="img">
                          <img src="image/feature.png" alt="" />
                      </div>
                  </div>
              </div>

          </div>
      </div>

  );
}

export default Feature;
