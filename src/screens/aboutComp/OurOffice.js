import React from 'react';

function OurOffice() {
  return (
      <div className="office">
          <div className="text-center mb-5">
              <h1>Our Office</h1>
              <p>Take a look inside the work Environment of</p>
              <p className='name'>My Shop</p>
          </div>
          <div className="our-office">

              <div className="container">

                  <div className="img">
                      <img src="image/office1.png" alt="" />
                      <img src="image/office2.png" alt="" />
                      <img src="image/office3.png" alt="" />
                      <img src="image/office4.png" alt="" />
                      <img src="image/office5.png" alt="" />
                  </div>
              </div>
          </div>

      </div>
  );
}

export default OurOffice;
