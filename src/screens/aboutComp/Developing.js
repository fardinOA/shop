import React from 'react';

function Developing() {
  return (
      <div className="developing ">
          <div className="container p-5">
              <div className="row">
                  <div className="col-sm-12 col-lg-5">
                      <h1>Developing Commerce Through Technology For Every-retailer</h1>
                  </div>
                  <div className="col-sm-12 col-lg-2 hv">

                  </div>
                  <div className="col-sm-12 col-lg-5">
                      <p>“Shopkpr” is a full-stack B2B startup that will use technology, data science, and design to connect unorganized retailers, brands, and multi-brand companies directly in one platform for re-engineering the medicine value chain in Bangladesh.</p>
                  </div>
              </div>

              <div className="row mt-5">
                  <div className="card col-sm-12 col-lg-4">
                      <div className="img ">
                          <img src="image/about-mission.png" alt="" />
                      </div>
                      <div className="p-1 content">
                          <h4>Mission</h4>
                          <p>To become the no.1 trustworthy digital retail operating system platform</p>
                          <a href="">Learn More</a>
                      </div>
                  </div>
                  <div className="card col-sm-12 col-lg-4">
                      <div className="img">
                          <img src="image/about-mission.png" alt="" />
                      </div>
                      <div className="p-1 content">
                          <h4>Mission</h4>
                          <p>To become the no.1 trustworthy digital retail operating system platform</p>
                          <a href="">Learn More</a>
                      </div>
                  </div>
                  <div className="card col-sm-12 col-lg-4">
                      <div className="img">
                          <img src="image/about-mission.png" alt="" />
                      </div>
                      <div className="p-1 content">
                          <h4>Mission</h4>
                          <p>To become the no.1 trustworthy digital retail operating system platform</p>
                          <a href="">Learn More</a>
                      </div>
                  </div>
              </div>
          </div>
      </div>

  );
}

export default Developing;
