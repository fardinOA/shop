import React from 'react';

function Grow() {
  return (
      <div className="grow">
          <div className="container">
              <div className="text-center">
                  <h1>How We Grow</h1>
                  <p>We offer wide range of products in categories such as
                      FMCG, Books, garments products and so on.</p>
              </div>
              <div>
                  <div className="grow-progress m-5 ">
                      {'<'} <div className="twinty">
                          2020, Sep
                      </div>
                      <div className="twinty-one">
                          2021, Jan
                      </div>
                      <div className="twinty-one">
                          2021, March
                      </div>
                      <div className="twinty-one">
                          2021, May
                      </div>
                      <div className="twinty-one">
                          Present
                      </div>
                      {'>'}
                  </div>
              </div>

              <div className="row mb-5">
                  <div className="col-sm-12 col-lg-6">
                      <div className="img">
                          <img src="image/grow-image.png" alt="" />
                      </div>
                  </div>
                  <div className="col-sm-12 col-lg-6">
                      <h5>Here is a brief run down of uour journey so far.</h5>
                      <p>The birth of the shopkpr concept,</p>
                      <p>In 2022 tow young and wnthusiastic minds came up with the concept which has the potential to change the supply chain industy in Bangladesh</p>
                      <div className='about row mt-5'>
                          <h4 className='signature col-sm-12 col-lg-4'>Shahed Ador</h4>
                          <h6 className='col-sm-12 col-lg-4'>Name <br /> Co-Founder & CEO </h6>
                          <h6 className='col-sm-12 col-lg-4'>Name <br /> <span>26.07.2021</span> </h6>
                      </div>
                  </div>
              </div>

          </div>
      </div>

  );
}

export default Grow;
