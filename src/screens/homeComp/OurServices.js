import React from 'react';
import serviceCard from '../../services'
function OurServices() {
  return (
      <div className="services">
          <div className="container">
              <div className="py-5 text-center">
                  <h1>Our Services</h1>
                  <h6>We offer a wide range of products in categories such as
                      FMCG, books garments products and so on.</h6>
              </div>
              <div className="row">


                  {
                      serviceCard.map((ele, ind) => (
                          <div className="card col-sm-12 col-lg-4 mb-5">
                              <i className={ele.icon}></i>
                              <div key={ind} className="card-body">
                                  <h3>{ele.title}</h3>
                                  <p>{ele.body}</p>
                                  <a href="#">Learn More</a>
                              </div>
                          </div>
                      ))
                  }

              </div>
          </div>
      </div>
  );
}

export default OurServices;
