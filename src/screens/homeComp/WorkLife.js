import React from 'react';

function WorkLife() {
  return (
      <div className="work-life container mb-5">
          <div className=" row">
              <div className=" col-sm-12 col-lg-7 text-center">
                  <h1>Work life at My Shop</h1>
                  <p>Its height of 16 miles (25 kilometers) makes it nearly three times the height
                      of Earth's Mount Everest, which is about 5.5 miles (8.9 km) high. Olympus Mons
                      is a gigantic shield volcano, which was formed after lava slowly crawled down its slopes.</p>
                  <p>of Earth's Mount Everest, which is about 5.5 miles (8.9 km) high. Olympus Mons of Earth's Mount Everest, which is about 5.5 miles (8.9 km) high. Olympus Mons</p>

                  <span><h5>Join us to make our dream a reality</h5></span>
                  <button className="btn worklife-btn">Learn More</button>
              </div>

              <div className=" col-sm-12 col-lg-5">
                  <div className="img row text-center">
                      <img className="col-sm-12 col-lg-6" src="image/group1.png" alt="" />
                      <img className="col-sm-12 col-lg-6" src="image/group2.png" alt="" />
                      <img className="col-sm-12 col-lg-6" src="image/group3.png" alt="" />

                  </div>
              </div>
          </div>
      </div>
  );
}

export default WorkLife;
