import React from 'react';

function Insight() {
  return (
      <div className="retaler-insight container mb-5">
          <div className="text-center mt-5 mb-5">
              <h1> Retailer's Insight</h1>
              <p>Here are some feedbacks from our valuable retailers about
                  their experience working with Shopkpr</p>
          </div>
          <div className="row">
              <div className="col-sm-12 col-lg-6 img">
                  <img src="image/insight.jpg" alt="" />
              </div>
              <div className="crd col-sm-12 col-lg-6 text-center">
                  <div className="img2">
                      <img className="rounded-circle" src="image/nur-alam.jpg" alt="" />
                  </div>
                  <h2>Nur Alom</h2>
                  <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nostrum facilis ut perferendis adipisci odio harum, hic reprehenderit, at laboriosam doloremque nemo asperiores architecto modi qui soluta corrupti aperiam, voluptatem labore?</p>

              </div>
          </div>
      </div>
  );
}

export default Insight;
