import React from 'react';

function MeetOurTeam() {
  return (
      <div className="our-team container">

          <div className="title text-center">
              <h1>Meet Our Team</h1>
              <p>Our leaders who made shopkpr possible.</p>
          </div>

          <div className="row">
              <div className="card col-sm-12 col-lg-6">
                  <div className="img">
                      <img className="" src="image/mubassir.jpg" alt="" />
                  </div>
                  <div className="about text-center">
                      <h4 className="mt-2">Measbual Shaded Ador</h4>
                      <p>Co-Founder & CEO</p>
                      <div className="">
                          <a href=""> <i class="fab fa-facebook-f m-2 "></i></a>

                          <a href=""> <i class="fab fa-linkedin-in "></i></a>
                      </div>
                  </div>
              </div>

              <div className="card col-sm-12 col-lg-6">
                  <div className="img">
                      <img className="" src="image/walid.jpg" alt="" />
                  </div>
                  <div className="about text-center">
                      <h4 className="mt-2">Walid Hasan</h4>
                      <p>Co-Founder & COO</p>
                      <div className="">
                          <a href=""> <i class="fab fa-facebook-f m-2 "></i></a>

                          <a href=""><i class="fab fa-linkedin-in "></i></a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  );
}

export default MeetOurTeam;
