import React from 'react';

function OurPartners() {
  return (
     <div className="partners">
          <div className="text-center mb-5">
              <h1>Our Partners</h1>
          </div>
          <div className="our-partners">
             
              <div className="container">

                  <div className="img">
                      <img src="image/p1.jpg" alt="" />
                      <img src="image/p2.png" alt="" />
                      <img src="image/p3.jpg" alt="" />
                      <img src="image/p4.png" alt="" />
                      <img src="image/p5.png" alt="" />
                      <img src="image/p6.png" alt="" />
                      <img src="image/p7.png" alt="" />
                      <img src="image/p8.png" alt="" />
                      <img src="image/p9.jpg" alt="" />
                      <img src="image/p10.png" alt="" />
                      <img src="image/p11.png" alt="" />
                  </div>
              </div>
          </div>
     </div>
  );
}

export default OurPartners;
