import React from 'react';

function HeroSection() {
  return (
     <div className=" hero-container">
         <div className="container">
              <div className="flip-card">
                 <div>
                      <div className="flip-card-content">
                          <div className="flip-card-inner">
                              <div className="flip-card-front row">
                                  <div className="content col-sm-12 col-lg-6">
                                      <h1>We work towards empowering your <span>Bussiness</span> </h1>
                                      <p>ShopKpr is one of the most efficient and largest Small-Bussiness finance & supply chain platform and emprowers millions of retailers in Bangladesh.</p>
                                      <button className="btn btn-banner">Download</button>
                                  </div>
                                  <div className="banner-image col-sm-12 col-lg-6 text-center">
                                      <img src="image/banner-image1.png" alt="" />
                                  </div>
                              </div>
                              <div className="flip-card-back row">
                                  <div className="content col-sm-12 col-lg-6">
                                      <h1>We work towards empowering your <span>Bussiness</span> </h1>
                                      <p>ShopKpr is one of the most efficient and largest Small-Bussiness finance & supply chain platform and emprowers millions of retailers in Bangladesh.</p>
                                      <button className="btn btn-banner">Download</button>
                                  </div>
                                  <div className="banner-image col-sm-12 col-lg-6 text-center">
                                      <img src="image/banner-image2.png" alt="" />
                                  </div>
                              </div>
                          </div>
                      </div>
                 </div>
              </div>
         </div>
     </div>
  );
}

export default HeroSection;
