import React from 'react';
import { Link } from 'react-router-dom'
function WhatIsMyShop() {
  return (
      <div className="what-is-shopKpr p-5">
         <div className="">
              <div className="row">
                  <div className="banner col-sm-12 col-lg-6 text-center">
                      <h1>What Is My Shop</h1>
                      <p>we offer a wide range of products in categories such as FMCG, Books, garments product and so on.</p>

                      <div className="img">
                          <img src="image/what_is_shopKpr.png" alt="" />
                      </div>
                  </div>
                  <div className="shopKpr-cards col-sm-12 col-lg-6">
                      <div className="card-container p-3">
                          <div className="card p-4 m-3">
                              <h3>Company</h3>
                              <p>We work with distributing companies who will send the products to Shopkpr.</p>
                          </div>
                          <div className="card p-4 m-3">
                              <h3>Shopkeeper</h3>
                              <p>We take the products and distribute them according to the retails needs.</p>
                          </div>
                          <div className="card p-4 m-3">
                              <h3>Local Pickup and Delivery</h3>
                              <p>We Deliver all the supplies to our retailers who use our <span><Link className='ancor'>shopKpr-cards</Link></span></p>
                          </div>
                          <div className="card p-4 m-3">
                              <h3>Retailers</h3>
                              <p>The retailor receive the product and access many other services which are offered by shopkpr</p>
                          </div>

                      </div>
                  </div>
              </div>
         </div>
      </div>
  );
}

export default WhatIsMyShop;
