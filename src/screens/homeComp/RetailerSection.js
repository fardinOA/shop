import React from 'react';

function RetailerSection() {
  return (
      <div className="retailer container mt-5">
          <div className="content row">
              <div className="content-item col-sm-12 col-lg-6 text-center">
                  <h2>Ensuring the development of commerce through technology or every retailer.</h2>
              </div>
              <div className="content-item2 col-sm-12 col-lg-6 text-center ">
                  <div className='row'>
                      <div className="col-lg-4 col-sm-12 text-center "> 
                          <h1>3K</h1>
                           <hr />
                          <h3> Retailers</h3>
                      </div>
                      <div className="col-lg-4 col-sm-12 text-center ">
                           <h1>10K</h1>
                           <hr />
                          <h3>Delivery</h3>
                        </div>
                      <div className="col-lg-4 col-sm-12 text-center"> 
                      <h1>5</h1>
                      <hr />
                          <h3> Towns</h3>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  );
}

export default RetailerSection;
