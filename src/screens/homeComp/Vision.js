import React from 'react';

function Vision() {
  return (
      <div className="vision container mb-5">
          <div className="row">
              <div className=" col-sm-12 col-lg-6">
                  <div className="content">
                      <h1>Vision to Grow,Everyday.</h1>
                      <p>Our operation is currently based in two towns which enabled us to serve more than 10,000 customers.</p>
                      <br />
                      <p>We plan to expane our dalivery services in Dhaka, Chittagong, Sylhet, Rajshahi, Khulna. Our expantion is planned to target all types of big and small cities.</p>
                      <button className="btn btn-vision">Explore Now</button>
                  </div>

              </div>
              <div className="banner col-sm-12 col-lg-6">
                  <div className="img">
                      <img src="image/banner.png" alt="" />
                  </div>
              </div>
          </div>
      </div>
  );
}

export default Vision;
