import React from 'react';
import Help from './careersComp/Help';
import Value from './careersComp/Value';
import EmployeThought from './teamComp/EmployeThought';

function Careers() {
  return (
    <div>
          <div className="career-hero">
              <div className="img">
                  <div>
                      <h3>Home / <span className="text-green">Careers</span></h3>
                  </div>
              </div>

          </div>
        
          <Help />
      
         <EmployeThought />
         <Value />

     
        <div className="join-us p-5">
            <div className="text-center">
                <h6>Get start now</h6>
                <br />
                <h1>Let's Work Together!</h1>
                <br />
                <button className="btn btn-lg">Join Us</button>
            </div>
        </div>





    </div>
  );
}

export default Careers;
