import React from 'react';

function Help() {
  return (
      <div className="help mt-5 mb-5">
          <div className="container">
              <div className="row">
                  <div className="col-sm-12 col-lg-6 mt-5">
                      <h1>Help us create the <br />
                          future of supply <br /> chain
                          in Bangladesh</h1>
                      <p>Using technology, AI and data science - we will take the supply chain and fintech to the next level.</p>
                      <div className="career-list mb-4">
                          <h4><i class="fas fa-check-circle"></i> A touch of innovation in business</h4>
                          <h4><i class="fas fa-check-circle"></i> Further progress in business and easy availability</h4>
                          <h4><i class="fas fa-check-circle"></i> A touch of business modernity</h4>
                      </div>
                      <button className="btn mb-3">Learn More</button>
                  </div>
                  <div className="col-sm-12 col-lg-6">
                      <div className="img">
                          <img src="image/help.png" alt="" />
                      </div>
                  </div>
              </div>
          </div>
      </div>

  );
}

export default Help;
