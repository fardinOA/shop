import React from 'react';

function Value() {
  return (
      <div className="our-value mb-5">
          <div className="container">
              <div className="row mb-5">
                  <div className="col-sm-12 col-lg-6 mt-5">
                      <div className="img">
                          <img src="image/feature.png" alt="" />
                      </div>
                  </div>
                  <div className="col-sm-12 col-lg-6 mt-5">
                      <h1>Our Values</h1>
                      <p>1. Making Customer satisfaction our priority. <br />
                          2. Making a positive impact on the supply chain.<br />
                          3. Focusing on work ethics and expanding Shopkpr.</p>
                      <div className="row">
                          <div className="col-sm-12 col-lg-6 card p-5">
                              <div >
                                  <i class="m-4 fas fa-user-secret"></i>
                                  Customer Driven
                              </div>
                          </div>
                          <div className="col-sm-12 col-lg-6 card p-5">
                              <div >
                                  <i class="m-4 fas fa-user-secret"></i>
                                  Dare to be Different
                              </div>

                          </div>
                          <div className="col-sm-12 col-lg-6 card p-5">
                              <div >
                                  <i class="m-4 fas fa-user-secret"></i>
                                  Helpline Support
                              </div>

                          </div>
                          <div className="col-sm-12 col-lg-6 card p-5">
                              <div >
                                  <i class="m-4 fas fa-user-secret"></i>
                                  Build Together
                              </div>

                          </div>
                      </div>
                  </div>
              </div>


              <div className="row mb-5">
                  <div className="col-sm-12 col-lg-6">
                      <h1>Why join My Shop</h1>
                      <p>Shopkpr is filled with many opportunities for your career. We are always looking for passionate individuals who can share their skills and make a positive impact. If you think you have what it takes, join us and be a part of our amazing team!</p>
                      <div className="row">
                          <div className="col-sm-12 col-lg-6 card p-5">
                              <div >
                                  <i class="m-4 fas fa-user-secret"></i>
                                  Customer Driven
                              </div>
                          </div>
                          <div className="col-sm-12 col-lg-6 card p-5">
                              <div >
                                  <i class="m-4 fas fa-user-secret"></i>
                                  Dare to be Different
                              </div>

                          </div>
                          <div className="col-sm-12 col-lg-6 card p-5">
                              <div >
                                  <i class="m-4 fas fa-user-secret"></i>
                                  Helpline Support
                              </div>

                          </div>
                          <div className="col-sm-12 col-lg-6 card p-5">
                              <div >
                                  <i class="m-4 fas fa-user-secret"></i>
                                  Build Together
                              </div>

                          </div>
                      </div>
                  </div>
                  <div className="col-sm-12 col-lg-6">
                      <div className="img">
                          <img src="image/career-why.png" alt="" />
                      </div>
                  </div>

              </div>

          </div>
      </div>


  );
}

export default Value;
