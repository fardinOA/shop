import React from 'react';
import {Link} from 'react-router-dom'
import serviceCard from '../services'
import HeroSection from './homeComp/HeroSection';
import Insight from './homeComp/Insight';
import MeetOurTeam from './homeComp/MeetOurTeam';
import OurPartners from './homeComp/OurPartners';
import OurServices from './homeComp/OurServices';
import RetailerSection from './homeComp/RetailerSection';
import Vision from './homeComp/Vision';
import WhatIsMyShop from './homeComp/WhatIsMyShop';

import WorkLife from './homeComp/WorkLife';

function Home() {
  return (
    <div>
          <HeroSection />
          <RetailerSection />
          <WhatIsMyShop />
          <Vision />
          <OurServices />
          <Insight />
          <MeetOurTeam />
          <OurPartners />
          <WorkLife />
           

    </div>
      
  );
}

export default Home;

