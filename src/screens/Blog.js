import React from 'react';
import PressRelease from './blogComp/PressRelease';

import BloagHero from './blogComp/BloagHero';
import PopularNews from './blogComp/PopularNews';
import WorkLife from './homeComp/WorkLife';
function Blog() {
  return (
    <div>
       <BloagHero />
       <PressRelease />
       <PopularNews />
       <WorkLife />
    </div>
  );
}

export default Blog;
