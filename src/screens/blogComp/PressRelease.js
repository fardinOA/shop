import React,{useState,useEffect} from 'react';
import PressSideBar from './PressSideBar';
import press_card from '../../pressCards'
import pagE from './Pagination'
function PressRelease() {

    const [page,setPage]=useState(1);
    const [array,setArray]=useState([]);

  let arr =[];
    useEffect(()=>{
        // console.log("from");
      arr= pagE(page);
      setArray(arr);
    
        
      
    },[page])

    // console.log(arr);

  return (
      <div className="press-release">
          <div className="text-center m-5">
              <h1>Press Release</h1>
              <p>We post blogs every now and then on crucial business topics</p>
          </div>
          <div className="container">
              <div className="row">
                  <div className="col-sm-12 col-lg-9 mt-3">
                      {
                         array.map((ele,ind)=>(
                              <div key={ind}className="card">
                                  <div className="img">
                                      <img src={ele.image} alt="" />
                                  </div>
                                  
                                 <div className="px-3">
                                      <h4>{ele.title}</h4>
                                      <p>{ele.details}</p>
                                 </div>
                              </div>
                          ))
                      }
                        <div className="text-center">
                            <h5>Page {page}</h5>
                        </div>
                      <div className="pageButton mb-4">
                          {
                              page === 1 ? <button onClick={() => setPage(page - 1)} disabled className="btn">Prev</button> :
                          <button onClick={() => setPage(page-1)}  className="btn">Prev</button>
                        
                          }
                          {
                              page === (press_card.length/3) ? <button onClick={() => setPage(page +1)} disabled className="btn">Next</button> :
                                  <button onClick={() => setPage(page +1)} className="btn">Next</button>

                          }
                        
                      </div>
                  </div>
                  <div className="col-sm-12 col-lg-3">
                      
                        <PressSideBar />
                  </div>
              </div>
          </div>
      </div>

  );
}

export default PressRelease;
