import press_card from '../../pressCards'

export default function(page){
    let arr = [];
    let k = 0;
    if (page >= 0 && (press_card.length >= page * 3)) {
        for (let i = (page * 3) - 3; i < page * 3; i++) {
            arr[k] = press_card[i];
            k++;
        }
    }
   
    return arr;
}