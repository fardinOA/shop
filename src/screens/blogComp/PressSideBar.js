import React from 'react';

function PressSideBar() {
  return (
    <div className="sidebar">
          <div className="search-box card mt-3 mb-3">
              <input type="text" placeholder="Search..." />
          </div>
         <div className="content">
              <h4 className="px-3">Category</h4>
              <div className="card mb-3">

                  <ul>
                      <li><i class="fas fa-greater-than"></i> Bussiness</li>
                      <li><i class="fas fa-greater-than"></i> Digital Bangladesh</li>
                      <li><i class="fas fa-greater-than"></i> Finance</li>
                      <li><i class="fas fa-greater-than"></i> General</li>
                      <li><i class="fas fa-greater-than"></i> People</li>
                      <li><i class="fas fa-greater-than"></i> Retail</li>
                      <li><i class="fas fa-greater-than"></i> Uncategorized</li>
                  </ul>
              </div>
         </div>

        <div className="content">
              <h4 className='px-3'>What's new</h4>
              <div className="card mb-3">
                  
                  <ul>
                      <li><i class="fas fa-greater-than"></i> This is the new way</li>
                      <li><i class="fas fa-greater-than"></i> Try to work with skills</li>
                      <li><i class="fas fa-greater-than"></i> Talk is cheap show me the code</li>
                      <li><i class="fas fa-greater-than"></i> Nothing</li>
                  </ul>
              </div>
        </div>

    </div>
  );
}

export default React.memo(PressSideBar);
