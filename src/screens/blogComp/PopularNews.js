import React from 'react';
import allNews from '../../popularNews'
function PopularNews() {
  return (
      <div className="popular-news mt-5">
          <div className="container">
              <div className="row">
                  {
                      allNews.map((ele, ind) => (
                          <div key={ind} className="card col-sm-12 col-lg-3 mb-5">
                              <div className="img m-auto">
                                  <img src={ele.image} alt="" />
                              </div>
                              <div className="text-center">
                                  <a href="">{ele.details}</a>
                              </div>
                          </div>
                      ))
                  }
              </div>
          </div>
      </div>


  );
}

export default PopularNews;
