import React from 'react'

import { Link } from 'react-router-dom'



function Navbar() {

    return (
        <nav className="navbar styleByMe navbar-expand-md navbar-dark bg-dark fixed-top text-center p-3">

            <div className="container ">
               
                    <Link to="/" className="navbar-brand justify-content-center">
                        My Shop
                    </Link>
                    <button className="navbar-toggler nav-button" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="text-dark navbar-toggler-icon"></span>
                    </button>
               
                <div className="NL collapse navbar-collapse bg-dark " id="navbarSupportedContent">
                    <div>
                        <ul className="navbar-nav">
                            <li className="nav-item"> <Link className="nav-link ancor" to='/'>Home</Link> </li>
                            <li className="nav-item"> <Link className="nav-link ancor" to='/about'>About</Link> </li>
                            <li className="nav-item">  <Link className="nav-link ancor" to='/team'>Team</Link> </li>
                            <li className="nav-item"> <Link className="nav-link ancor" to='/blog'>Blog</Link> </li>
                            <li className="nav-item"> <Link className="nav-link ancor" to='careers'>Careers</Link> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    )
}

export default Navbar



