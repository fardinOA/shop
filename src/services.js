const serviceCard=[
    {
        icon:'fas fa-flask text-center mt-4',
        title: 'Effective supply chain',
        body:'We ensure super fast delivery We ensure super fast delivery service through our own delivery system through multi brand store front delivery',

    },
    {
        icon: 'fas fa-flask text-center mt-4',
        title: 'Free Digital Khata',
        body: 'We have an organized free digital khata so you can have everything in one spot',

    },
    {
        icon: 'fas fa-flask text-center mt-4',
        title: 'Payment Method',
        body: 'Check out our flexible and hasse free payment methods ensuring smooth shopping experience',

    },
    {
        icon: 'fas fa-flask text-center mt-4',
        title: 'Financial Support',
        body: 'We provide aid towards small retailors that could help boosting up their business.',

    },
    {
        icon: 'fas fa-flask text-center mt-4',
        title: 'Reducing Cost',
        body: 'We eliment the middle man and help companies to reduce third party cost.',

    },
    {
        icon: 'fas fa-flask text-center mt-4',
        title: 'Nation Wide Delivery',
        body: 'We are expanding our company to provide nationwide delivery all over Bangladesh.',

    },
]

export default serviceCard