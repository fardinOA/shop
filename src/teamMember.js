 const team_member=[
    {
        image:'image/team1.png',
        name:'Tasnimah Afrin',
        post:'Human Resource Manager',
         details:`Assalamu Alaikum, I am the lead HR of Shopkpr. Everyone on the whole team loves making new things and new revelations. It's like the beginning of a new fairy tale.`
    },
    {
        image: 'image/team2.png',
        name: 'Amimul Ehsan Rahi',
        post: 'Android and Backend Developer',
        details:`I have been working here for a few days but everyone here is very friendly, very good people and the working environment is very good. Thanks, Shopkpr`
    },
    {
        image: 'image/team3.png',
        name: 'Abu Hasan',
        post: 'Web Developer',
        details:`Assalamu Alaikum, I am the lead web developer of Shopkeeper. Everyone on the whole team loves making new things and new revelations. It's like the beginning of a new fairy tale.`
    },
    {
        image: 'image/team4.png',
        name: 'Tariqul Islalm Tanu',
        post: 'Android and Backend Developer',
        details:`I am the Frontend Web developer of Shopkpr. Everyone on the whole team loves making new things and new revelations. It's like the beginning of a new fairy tale.`
    },
    {
        image: 'image/team5.png',
        name: 'Mehedi Hasan Khairul',
        post: 'Frontend Developer',
        details:`I am the lead Android and Backend Developer of Shopkpr. Everyone on the whole team loves making new things and new revelations. It's like the beginning of a new fairy tale. Thanks, Shopkpr.`
    },
    {
        image: 'image/team6.png',
        name: 'Shah Faiza Zarin',
        post: 'UI Designer',
        
    },
    {
        image: 'image/team7.png',
        name: 'Soykot Talukdar',
        post: 'UI/UX Designer',
        details:`I have been working here for a few days but everyone here is very friendly, very good people and the working environment is very good. Thanks, Shopkpr.`
    },
]

export default team_member;