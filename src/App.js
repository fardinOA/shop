import { BrowserRouter, Route } from "react-router-dom";
import "./App.css";
import Navbar from "./screens/Navbar";
import Footer from "./screens/Footer";
import Home from "./screens/Home";
import About from "./screens/About";
import Team from "./screens/Team";
import Blog from "./screens/Blog";
import Careers from "./screens/Careers";
import React, { useEffect, useState, Component } from "react";
 
function App({ props }) {
    const [latitude, setLatitude] = useState("");
    const [longitude, setLongitude] = useState("");

    useEffect(() => {
        navigator.geolocation.getCurrentPosition((pos) => {
            setLatitude(pos.coords.latitude);
            setLongitude(pos.coords.longitude);
        });
    }, []);
    console.log(latitude, longitude);
    return (
        <BrowserRouter className="App">
            <header>
                <Navbar />
            </header>

            <main>
                <Route path="/" exact={true} component={Home}></Route>
                <Route path="/about" component={About}></Route>
                <Route path="/team" component={Team}></Route>
                <Route path="/blog" component={Blog}></Route>
                <Route path="/careers" component={Careers}></Route>
            </main>

            <footer>
                <Footer></Footer>
            </footer>
        </BrowserRouter>
    );
}

export default App;
