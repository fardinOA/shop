const press_card=[
    {
        image:'image/press1.png',
        title:'01. Want to start a business? Learn the three steps of planning.',
        details:`You want to do business but have no big business plan? Don't worry, you're not alone. The desire to do business is always greater than a successful business plan. I say this from my own experience and the whole thing becomes very frustrating for us when I see others come up with one business plan af...`
    },
    {
        image:'image/press2.png',
        title:`02. This week's top 5 news in the small business and startup world`,
        details:`Moneycontrol in collaboration with Cisco is hosting the Small Business Summit; New streaming features to help build customer confidence in retail, food and hospitality; IAMAI's Virtual Incubator activities to support small town tech startups; Coronary Crisis Voluntary ‘Assistance’ Project; With Mast...`
    },
    {
        image:'image/press3.png',
        title:`03. 3 Start-up Challenges Every Entrepreneur Must Overcome`,
        details: `From a handful of tech companies to thousands of new ventures, India's start- up ecosystem has come a long way in the past decade.According to the report published by NASSCOM, ‘India ranks 3rd globally, with more than 4, 200 start - ups, creating more than 80, 000 jobs’.The Indian start - up ecosystem re...`
    }
    ,
    {
        image: 'image/press4.jpg',
        title: `04. BUILD A GREAT BUSINESS YOU LOVE— IT DOESN'T MATTER IF IT'S A STARTUP OR AN SME!`,
        details: `In Bangladesh, the term "SME" is not considered good. Why? SMEs are the backbone of this country—and many of them have the potential to become multi-million dollar companies. People often forget that a "small or medium enterprise" can grow to become a large enterprise and then an even larger, very...`
    },
    {
        image: 'image/press4.jpg',
        title: `05. BUILD A GREAT BUSINESS YOU LOVE— IT DOESN'T MATTER IF IT'S A STARTUP OR AN SME!`,
        details: `In Bangladesh, the term "SME" is not considered good. Why? SMEs are the backbone of this country—and many of them have the potential to become multi-million dollar companies. People often forget that a "small or medium enterprise" can grow to become a large enterprise and then an even larger, very...`
    },
    {
        image: 'image/press4.jpg',
        title: `06. BUILD A GREAT BUSINESS YOU LOVE— IT DOESN'T MATTER IF IT'S A STARTUP OR AN SME!`,
        details: `In Bangladesh, the term "SME" is not considered good. Why? SMEs are the backbone of this country—and many of them have the potential to become multi-million dollar companies. People often forget that a "small or medium enterprise" can grow to become a large enterprise and then an even larger, very...`
    },
    {
        image: 'image/press4.jpg',
        title: `07. BUILD A GREAT BUSINESS YOU LOVE— IT DOESN'T MATTER IF IT'S A STARTUP OR AN SME!`,
        details: `In Bangladesh, the term "SME" is not considered good. Why? SMEs are the backbone of this country—and many of them have the potential to become multi-million dollar companies. People often forget that a "small or medium enterprise" can grow to become a large enterprise and then an even larger, very...`
    },
    {
        image: 'image/press4.jpg',
        title: `08. BUILD A GREAT BUSINESS YOU LOVE— IT DOESN'T MATTER IF IT'S A STARTUP OR AN SME!`,
        details: `In Bangladesh, the term "SME" is not considered good. Why? SMEs are the backbone of this country—and many of them have the potential to become multi-million dollar companies. People often forget that a "small or medium enterprise" can grow to become a large enterprise and then an even larger, very...`
    },
    {
        image: 'image/press4.jpg',
        title: `09. BUILD A GREAT BUSINESS YOU LOVE— IT DOESN'T MATTER IF IT'S A STARTUP OR AN SME!`,
        details: `In Bangladesh, the term "SME" is not considered good. Why? SMEs are the backbone of this country—and many of them have the potential to become multi-million dollar companies. People often forget that a "small or medium enterprise" can grow to become a large enterprise and then an even larger, very...`
    },
    {
        image: 'image/press4.jpg',
        title: `10. BUILD A GREAT BUSINESS YOU LOVE— IT DOESN'T MATTER IF IT'S A STARTUP OR AN SME!`,
        details: `In Bangladesh, the term "SME" is not considered good. Why? SMEs are the backbone of this country—and many of them have the potential to become multi-million dollar companies. People often forget that a "small or medium enterprise" can grow to become a large enterprise and then an even larger, very...`
    },
    {
        image: 'image/press4.jpg',
        title: `11. BUILD A GREAT BUSINESS YOU LOVE— IT DOESN'T MATTER IF IT'S A STARTUP OR AN SME!`,
        details: `In Bangladesh, the term "SME" is not considered good. Why? SMEs are the backbone of this country—and many of them have the potential to become multi-million dollar companies. People often forget that a "small or medium enterprise" can grow to become a large enterprise and then an even larger, very...`
    },
    {
        image: 'image/press4.jpg',
        title: `12. BUILD A GREAT BUSINESS YOU LOVE— IT DOESN'T MATTER IF IT'S A STARTUP OR AN SME!`,
        details: `In Bangladesh, the term "SME" is not considered good. Why? SMEs are the backbone of this country—and many of them have the potential to become multi-million dollar companies. People often forget that a "small or medium enterprise" can grow to become a large enterprise and then an even larger, very...`
    }
]

export default press_card;